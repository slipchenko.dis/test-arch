using AppLogic.Model;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace AppLogic.Mouse
{
    [RequireComponent(typeof(MouseProperties))]
    public class MousePresenter : MonoBehaviour
    {
        public Action<ModelPresenter> SelectModel;
        public Action<ModelPresenter> PlayModelAudio;
        public Action<ModelPresenter> RotateModel;

        [SerializeField] MouseView mouseView;
        MouseProperties mouseProperties;

        void Start()
        {
            mouseProperties = GetComponent<MouseProperties>();

            SubscribeViewEvents();
        }

        private void Update()
        {
            MouseClick();
        }

        private void MouseClick()
        {
            if (Input.GetMouseButtonDown(0))
            {
                Ray ray = UnityEngine.Camera.main.ScreenPointToRay(Input.mousePosition);

                if (Physics.Raycast(ray, out var hit, Mathf.Infinity))
                {
                    if (hit.transform != null && hit.transform.TryGetComponent<Model.ModelPresenter>(out var modelPresenter))
                    {
                        ActionWithModel(modelPresenter);
                    }
                }
            }
        }

        private void ActionWithModel(Model.ModelPresenter modelPresenter)
        {
            switch (mouseProperties.State)
            {
                case MouseState.Selection:
                    SelectModel?.Invoke(modelPresenter);
                    break;
                case MouseState.Audio:
                    PlayModelAudio?.Invoke(modelPresenter);
                    break;
                case MouseState.Rotation:
                    RotateModel?.Invoke(modelPresenter);
                    break;
                default:
                    break;
            }

        }

        private void OnDestroy()
        {
            UnsubscribeViewEvents();
        }

        private void SubscribeViewEvents()
        {
            mouseView.AudioClicked += () => ToggleState(MouseState.Audio);
            mouseView.SelectionClicked += () => ToggleState(MouseState.Selection);
            mouseView.RotationClicked += () => ToggleState(MouseState.Rotation);
        }

        private void UnsubscribeViewEvents()
        {
            mouseView.AudioClicked -= () => ToggleState(MouseState.Audio);
            mouseView.SelectionClicked -= () => ToggleState(MouseState.Selection);
            mouseView.RotationClicked -= () => ToggleState(MouseState.Rotation);
        }

        private void ToggleState(MouseState mouseState)
        {
            mouseProperties.State = mouseState;
        }
    }
}
