using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace AppLogic.Mouse
{
    public class MouseObserver : MonoBehaviour
    {
        [SerializeField] MousePresenter mousePresenter;
        [SerializeField] PlayAudio.PlayAudioPresenter playAudioPresenter;
        [SerializeField] Selection.SelectionPresenter selectionPresenter;
        [SerializeField] Rotation.RotationPresenter rotationPresenter;

        private void Start()
        {
            EventsSubscribe();
        }

        private void OnDestroy()
        {
            EventsUnsubscribe();
        }

        private void EventsSubscribe()
        {
            mousePresenter.PlayModelAudio += playAudioPresenter.PlayModelAudio;
            mousePresenter.SelectModel += selectionPresenter.SelectModel;
            mousePresenter.RotateModel += rotationPresenter.RotateTheModel;
        }

        private void EventsUnsubscribe()
        {
            mousePresenter.PlayModelAudio -= playAudioPresenter.PlayModelAudio;
            mousePresenter.SelectModel -= selectionPresenter.SelectModel;
            mousePresenter.RotateModel -= rotationPresenter.RotateTheModel;
        }
    }
}

