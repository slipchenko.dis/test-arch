using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace AppLogic.Mouse
{
    public class MouseView : MonoBehaviour
    {
        public Action AudioClicked;
        public Action SelectionClicked;
        public Action RotationClicked;

        [SerializeField] Button audioButton;
        [SerializeField] Button selectionButton;
        [SerializeField] Button rotationButton;

        private void Start()
        {
            EventsSubscribe();
        }

        private void OnDestroy()
        {
            EventsUnsubscribe();
        }

        private void EventsSubscribe()
        {
            audioButton.onClick.AddListener(() => AudioClicked?.Invoke());
            selectionButton.onClick.AddListener(() => SelectionClicked?.Invoke());
            rotationButton.onClick.AddListener(() => RotationClicked?.Invoke());
        }

        private void EventsUnsubscribe()
        {
            audioButton.onClick.RemoveListener(() => AudioClicked?.Invoke());
            selectionButton.onClick.RemoveListener(() => SelectionClicked?.Invoke());
            rotationButton.onClick.RemoveListener(() => RotationClicked?.Invoke());
        }
    }
}

