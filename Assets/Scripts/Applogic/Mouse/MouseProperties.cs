using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace AppLogic.Mouse
{
    public enum MouseState
    {
        None,
        Selection,
        Audio,
        Rotation
    }

    public class MouseProperties : MonoBehaviour
    {
        public MouseState State;
    }
}
