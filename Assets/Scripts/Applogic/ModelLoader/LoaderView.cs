using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace AppLogic.ModelLoader
{
    public class LoaderView : MonoBehaviour
    {
        public Action LoadButtonClicked;

        [SerializeField] Button loadButton;

        private void Start()
        {
            EventsSubscribe();
        }

        private void OnDestroy()
        {
            EventsUnsubscribe();
        }

        private void EventsSubscribe()
        {
            loadButton.onClick.AddListener(() => LoadButtonClicked?.Invoke());
        }

        private void EventsUnsubscribe()
        {
            loadButton.onClick.AddListener(() => LoadButtonClicked?.Invoke());
        }
    }
}
