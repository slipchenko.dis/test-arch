using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace AppLogic.ModelLoader
{
    [RequireComponent(typeof(LoaderProperties))]
    public class LoaderPresenter : MonoBehaviour
    {
        public Action<Model.ModelPresenter> ModelLoaded;

        [SerializeField] LoaderView loaderView;
        LoaderProperties loaderProperties;

        private void Start()
        {
            loaderProperties = GetComponent<LoaderProperties>();
            SubscribeViewEvents();
        }

        private void OnDestroy()
        {
            UnsubscribeViewEvents();
        }

        private void SubscribeViewEvents()
        {
            loaderView.LoadButtonClicked += ()=> LoadModel(loaderProperties.Model);
        }

        private void UnsubscribeViewEvents()
        {
            loaderView.LoadButtonClicked -= () => LoadModel(loaderProperties.Model);
        }

        private void LoadModel(Model.ModelPresenter modelView)
        {
            var spawnedModel = Instantiate(modelView, Vector3.zero, Quaternion.identity);

            ModelLoaded?.Invoke(spawnedModel);
        }
    }
}

