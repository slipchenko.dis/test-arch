using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace AppLogic.Model
{
    [RequireComponent(typeof(ModelProperties))]
    [RequireComponent(typeof(ModelView))]
    public class ModelPresenter : MonoBehaviour
    {
        ModelProperties modelProperties;
        ModelView modelView;

        private void Start()
        {
            modelView = GetComponent<ModelView>();
            modelProperties = GetComponent<ModelProperties>();
        }

        public void SelectModel()
        {
            Debug.Log("Select");
        }

        public void PlayAudio()
        {
            Debug.Log("PlayAudio");
        }

        public void Rotation()
        {
            Debug.Log("Rotation");
        }
    }
}

