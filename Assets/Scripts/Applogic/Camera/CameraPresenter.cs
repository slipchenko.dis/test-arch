using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace AppLogic.Camera
{
    public class CameraPresenter : MonoBehaviour
    {
        public Action<Vector3> CameraPositionChanged;

        [SerializeField] UnityEngine.Camera camera;

        private void Update()
        {
            if (camera.transform.hasChanged)
            {
                CameraPositionChanged?.Invoke(camera.transform.position);
            }
        }

        public Vector3 GetCameraPosition()
        {
            return camera.transform.position;
        }
    }
}

