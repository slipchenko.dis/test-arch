using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KeyboardMoveController : MonoBehaviour
{
    [SerializeField]
    private Transform cameraTransform;

    [SerializeField]
    private Transform rotationCameraPivot;

    [Range(0, 1)]
    [SerializeField]
    private float movementSpeed;


    [Range(0, 5)]
    [SerializeField]
    private float rotationSpeed;

    void Update()
    {
        if (Input.GetKey(KeyCode.W))
        {
            Move(Vector3.forward);
        }
        else if (Input.GetKey(KeyCode.S))
        {
            Move(-Vector3.forward);
        }

        if (Input.GetKey(KeyCode.D))
        {
            Move(Vector3.right);
        }
        else if (Input.GetKey(KeyCode.A))
        {
            Move(-Vector3.right);
        }

        if (Input.GetKey(KeyCode.Q))
        {
            Move(-Vector3.up);
        }
        else if (Input.GetKey(KeyCode.E))
        {
            Move(Vector3.up);
        }

        if (Input.GetMouseButton(0))
            rotationCameraPivot.Rotate(0, Input.GetAxis("Mouse X") * rotationSpeed, 0);
    }

    private void Move(Vector3 movementDirection)
    {
        cameraTransform.position += movementDirection * movementSpeed;
    }
}
