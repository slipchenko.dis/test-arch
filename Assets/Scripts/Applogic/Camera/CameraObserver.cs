using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace AppLogic.Camera
{
    public class CameraObserver : MonoBehaviour
    {
        [SerializeField] CameraPresenter cameraPresenter;

        private Vector3 currentCameraPositionRoundedDown;

        private void Start()
        {
            EventsSubscribe();

            OnCameraPositionChanged(cameraPresenter.GetCameraPosition());
        }

        private void OnDestroy()
        {
            EventsUnsubscribe();
        }

        private void EventsSubscribe()
        {
            cameraPresenter.CameraPositionChanged += OnCameraPositionChanged;
        }

        private void EventsUnsubscribe()
        {
            cameraPresenter.CameraPositionChanged -= OnCameraPositionChanged;
        }

        private void OnCameraPositionChanged(Vector3 position)
        {
            var xRoundedDown = Math.Floor(position.x);
            var yRoundedDown = Math.Floor(position.y);
            var zRoundedDown = Math.Floor(position.z);

            var newCameraPositionRoundedDown = new Vector3((float)xRoundedDown, (float)yRoundedDown, (float)zRoundedDown);

            if(currentCameraPositionRoundedDown != newCameraPositionRoundedDown)
                Debug.Log("new camera rounded pos: " + newCameraPositionRoundedDown);

            currentCameraPositionRoundedDown = newCameraPositionRoundedDown;
        }
    }
}
