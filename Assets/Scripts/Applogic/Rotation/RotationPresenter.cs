using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace AppLogic.Rotation
{
    [RequireComponent(typeof(RotationProperties))]
    public class RotationPresenter : MonoBehaviour
    {
        public void RotateTheModel(Model.ModelPresenter modelPresenter)
        {
            Debug.Log("Model '" + modelPresenter.gameObject.name + "' rotated");
        }
    }
}

