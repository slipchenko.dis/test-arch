using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace AppLogic.PlayAudio
{
    [RequireComponent(typeof(PlayAudioProperties))]
    public class PlayAudioPresenter : MonoBehaviour
    {
        public void PlayModelAudio(Model.ModelPresenter modelPresenter)
        {
            Debug.Log("Play the audio of model '" + modelPresenter.gameObject.name + "'");
        }
    }
}

