using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace AppLogic.Selection
{
    [RequireComponent(typeof(SelectionProperties))]
    public class SelectionPresenter : MonoBehaviour
    {
        public void SelectModel(Model.ModelPresenter modelPresenter)
        {
            Debug.Log("Selected model '" + modelPresenter.gameObject.name + "'");
        }
    }
}